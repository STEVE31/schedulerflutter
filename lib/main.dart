import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/bottom%20nav.dart';
import 'package:flutter_app/profile.dart';
import 'package:fluttertoast/fluttertoast.dart';

void main() => runApp(MyApp());

/// This Widget is the main application widget.
class MyApp extends StatelessWidget {
  static const String _title = 'Flutter Code Sample';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: _title,
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primaryColor: Colors.amber,
        primaryColorLight: Colors.amberAccent,
        accentColor: Colors.amberAccent,
        primaryColorDark: Colors.amberAccent,
      ),
      home: MyStatefulWidget(),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  MyStatefulWidget({Key key}) : super(key: key);

  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  static int dex = 0;

  static List<Widget> widgs = <Widget>[
    Center(
      child: BottomNav(),
    ),
    Scaffold(
      backgroundColor: Colors.amber,
      body: Center(
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              topRight: Radius.circular(20),
            ),
            color: Colors.white,
          ),
          child: Center(
            child: TextField(
              decoration: InputDecoration(
                border: InputBorder.none,
                icon: Icon(Icons.alarm_add),
                labelText: 'Schedule Namer',
              ),
            ),
          ),
        ),
      ),
    ),
    Center(
      child: Profile(),
    ),
    Container(),
  ];

  //1
  Timer _timer;
  MaterialColor col = Colors.grey;

  ImageProvider i;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Scheduler'),
        centerTitle: true,
        elevation: 0,
      ),
      body: Center(
        child: widgs.elementAt(dex),
      ),
      drawer: Drawer(
        // Add a ListView to the drawer. This ensures the user can scroll
        // through the options in the drawer if there isn't enough vertical
        // space to fit everything.
        elevation: 7,
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Container(
                child: Column(
                  children: <Widget>[
                    Material(
                      borderRadius: BorderRadius.all(Radius.circular(50)),
                      child: Image(
                        height: 100,
                        width: 100,
                        image: NetworkImage(
                          'https://www.iowagcsa.org/resources/Pictures/Member-Login-Icon.png',
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(8),
                      child: Text('Username'),
                    )
                  ],
                ),
              ),
              decoration: BoxDecoration(
                //color: Colors.amberAccent,
                gradient: LinearGradient(
                  colors: <Color>[
                    Colors.white,
                    Colors.amberAccent,
                    Colors.amber[600],
                  ],
                ),
                //image: DecorationImage(image: i),
              ),
            ),
            nime(Icons.calendar_today, 'Schedules', () {
              setState(
                    () {
                  dex = 0;
                },
              );
              if (dex == 0) {
                this.col = Colors.amber;
              } else {
                this.col = Colors.grey;
              }
              Fluttertoast.showToast(
                  msg: 'Schedules',
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.BOTTOM,
                  timeInSecForIos: 1,
                  backgroundColor: Colors.blue,
                  textColor: Colors.white,
                  fontSize: 12.0);
              _timer = new Timer(const Duration(milliseconds: 700), () {
                Navigator.pop(context);
              });
            }, Colors.grey),
            nime(Icons.alarm_add, 'Scheduling', () {
              setState(() {
                dex = 1;
              });
              if (dex == 1) {
                this.col = Colors.amber;
              } else {
                this.col = Colors.grey;
              }
              Fluttertoast.showToast(
                  msg: 'Scheduling',
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.BOTTOM,
                  timeInSecForIos: 1,
                  backgroundColor: Colors.blue,
                  textColor: Colors.white,
                  fontSize: 12.0);
              _timer = new Timer(const Duration(milliseconds: 700), () {
                Navigator.pop(context);
              });
            }, Colors.grey),
            nime(Icons.account_box, 'Edit profile', () {
              setState(() {
                dex = 2;
              });
              if (dex == 2) {
                this.col = Colors.amber;
              } else {
                this.col = Colors.grey;
              }
              Fluttertoast.showToast(
                  msg: 'Edit profile',
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.BOTTOM,
                  timeInSecForIos: 1,
                  backgroundColor: Colors.blue,
                  textColor: Colors.white,
                  fontSize: 12.0);
              _timer = new Timer(const Duration(milliseconds: 700), () {
                Navigator.pop(context);
              });
            }, Colors.grey),
            nime(Icons.image, 'View Images', () {
              setState(
                    () {
                  dex = 3;
                },
              );
              Fluttertoast.showToast(
                  msg: 'View Images',
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.BOTTOM,
                  timeInSecForIos: 1,
                  backgroundColor: Colors.amber,
                  textColor: Colors.white,
                  fontSize: 12.0);
              if (dex == 3) {
                this.col = Colors.amber;
              } else {
                this.col = Colors.grey;
              }
              _timer = new Timer(const Duration(milliseconds: 700), () {
                Navigator.pop(context);
              });
            }, Colors.grey),
            /*ListTile(
              title: Text('Schedules'),
              leading: Icon(Icons.calendar_today),
              selected: dex == 0,
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                setState(() {
                  dex = 0;
                });
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Text('Scheduling'),
              leading: Icon(Icons.alarm_add),
              selected: dex == 1,
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                setState(() {
                  dex = 1;
                });
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Text('Edit profile'),
              leading: Icon(Icons.account_box),
              selected: dex == 2,
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                setState(() {
                  dex = 2;
                });
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Text('View Images'),
              leading: Icon(Icons.image),
              selected: dex == 3,
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                setState(() {
                  dex = 3;
                });
                Navigator.pop(context);
              },
            ),*/
          ],
        ),
      ),
    );
  }
}

class nime extends StatelessWidget {
  IconData icon;
  String text;
  Function func;
  MaterialColor col;

  nime(this.icon, this.text, this.func, this.col);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Padding(
      padding: EdgeInsets.fromLTRB(7, 0, 0, 0),
      child: InkWell(
        splashColor: Colors.amber,
        onTap: func,
        child: Container(
          height: 60,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Icon(
                    icon,
                    color: col,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8),
                    child: Text(
                      text,
                      style: TextStyle(
                        fontSize: 16,
                        color: col,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

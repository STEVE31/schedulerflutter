import 'package:flutter/material.dart';

class BottomNav extends StatefulWidget {
  @override
  BtmNv createState() {
    // TODO: implement createState
    return new BtmNv();
  }
}

class BtmNv extends State<BottomNav> {
  int _selectedIndex = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  static const List<Widget> _widgetOptions = <Widget>[
    Center(
      child: Text(
        'Pending Schedules',
        style: optionStyle,
      ),
    ),
    Center(
      child: Text(
        'All Schedules',
        style: optionStyle,
      ),
    ),
    Center(
      child: Text(
        'Past Schedules',
        style: optionStyle,
      ),
    ),
    Scaffold(
      body: Center(
        child: Text("My profile"),
      ),
    ),
  ];

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.amber,
      body: Center(
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              topRight: Radius.circular(20),
            ),
            color: Colors.white,
          ),
          child: _widgetOptions.elementAt(_selectedIndex),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(
              Icons.alarm_on,
            ),
            title: Text(
              '.',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20,
              ),
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.alarm),
            title: Text(
              '.',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20,
              ),
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.alarm_off),
            title: Text(
              '.',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20,
              ),
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_circle),
            title: Text(
              '.',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20,
              ),
            ),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.amber[700],
        onTap: _onItemTapped,
        elevation: 20,
        iconSize: 22,
        //fixedColor: Colors.purple,
        unselectedItemColor: Colors.grey[500],
      ),
    );
  }
}

import 'package:flutter/material.dart';

class Profile extends StatefulWidget {
  @override
  Prof createState() {
    // TODO: implement createState
    return new Prof();
  }
}

class Prof extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.amber,
      body: Center(
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              topRight: Radius.circular(20),
            ),
            color: Colors.white,
          ),
          child: Center(
            child: Padding(
              padding: EdgeInsets.only(
                top: 50,
              ),
              child: Column(
                children: <Widget>[
                  CircleAvatar(
                    radius: 60,
                    backgroundColor: Colors.white,
                    backgroundImage: NetworkImage(
                      'https://www.iowagcsa.org/resources/Pictures/Member-Login-Icon.png',
                    ),
                  ),
                  Text(
                    'Whot',
                    softWrap: true,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
